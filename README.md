
github-get-pr-descriptions-rest
===============================

Prints the number and description of all the pull requests merged
to a branch since an initial date.

Use the file *src/main/resources/config.properties* to set up the parameters:

```
githubApiUrl= https://api.github.com or the url for the API for Github Enterprise

organization= the organization the repository is in (it can be your username)

repository= the repository name

initialDate= get all pull requests from this date on - compared against updated_at field of pull request

authToken= if you need authenticated access to the API, paste the token here

baseBranch= as the name suggests, the branch which receives the pull requests.

certificate= if needed, configure here the name of SSL certificate you put in src/main/resources/
```
