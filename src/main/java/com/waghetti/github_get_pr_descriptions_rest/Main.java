package com.waghetti.github_get_pr_descriptions_rest;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.ParseException;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClients;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.client.methods.HttpGet;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

public class Main {

    private static final HashMap<String, String> properties = readProperties();
    private static final KeyStore keyStore = readKeyStore();
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");


    public static void main(String[] args) throws Exception {

        Date initialDate = DATE_FORMAT.parse(adjustDateString(properties.get("initialDate")));

        SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(keyStore, null)
                .build();

        HttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).build();
        Header header = new AuthorizationHeader();
        Gson gson = new Gson();

        try {
            HttpGet request = new HttpGet(createStringRequest());

            if (properties.get("certificate") != null) {
                request.addHeader(header);
            }

            HttpResponse response = httpClient.execute(request);
            ArrayList<LinkedTreeMap<String, Object>> pullRequests = (ArrayList<LinkedTreeMap<String, Object>>) gson.fromJson(new BasicResponseHandler().handleResponse(response), Object.class);
            for (LinkedTreeMap pullRequest : pullRequests) {
                if (initialDate.compareTo(DATE_FORMAT.parse(adjustDateString((String)pullRequest.get("updated_at")))) <= 0) {
                    System.out.print(pullRequest.get("number") + ": ");
                    System.out.println(pullRequest.get("title"));
                }
            }
        } catch (
                IOException exception) {
            System.out.println(exception.toString());
        }


    }

    private static String createStringRequest() {

        String apiUrl = properties.get("githubApiUrl");
        String repository = properties.get("repository");
        String organization = properties.get("organization");
        String baseBranch = properties.get("baseBranch");

        StringBuilder requestString = new StringBuilder();

        requestString.append(apiUrl);
        requestString.append("/repos/");
        requestString.append(organization);
        requestString.append("/");
        requestString.append(repository);
        requestString.append("/pulls?");

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("base", baseBranch);
        parameters.put("state", "closed");
        parameters.put("sort", "updated");

        for (String key : parameters.keySet()) {
            requestString.append(key);
            requestString.append("=");
            requestString.append(parameters.get(key));
            requestString.append("&");
        }

        return requestString.toString();

    }

    private static String adjustDateString(String date) {
        return date.replaceAll("Z", "+0000");
    }

    private static class AuthorizationHeader implements Header {
        @Override
        public HeaderElement[] getElements() throws ParseException {
            return new HeaderElement[0];
        }

        @Override
        public String getName() {
            return "Authorization";
        }

        @Override
        public String getValue() {
            return "Bearer " + properties.get("authToken");
        }

    }

    private static KeyStore readKeyStore() {

        KeyStore keyStore = null;

        try {
            String certificate = "/" + properties.get("certificate"); // or .p12

            Certificate cer;
            InputStream keyStoreStream = Main.class.getResourceAsStream(certificate);
            CertificateFactory fact = CertificateFactory.getInstance("X.509");
            cer = fact.generateCertificate(keyStoreStream);
            keyStore = KeyStore.getInstance("PKCS12"); // or "PKCS12"
            keyStore.load(null);
            keyStore.setCertificateEntry("github", cer);
        } catch (IOException | CertificateException | KeyStoreException | NoSuchAlgorithmException exception) {
            System.out.println(exception);
        }

        return keyStore;
    }

    private static HashMap<String, String> readProperties() {

        Properties properties = new Properties();
        InputStream inputStream = Main.class.getClassLoader().getResourceAsStream("config.properties");
        HashMap<String, String> propertyMap = new HashMap<>();

        try {
            if (inputStream != null) {
                properties.load(inputStream);
            }
        } catch (IOException exception) {
            // do nothing
        }

        propertyMap.put("githubApiUrl", properties.getProperty("githubApiUrl"));
        propertyMap.put("organization", properties.getProperty("organization"));
        propertyMap.put("repository", properties.getProperty("repository"));
        propertyMap.put("authToken", properties.getProperty("authToken"));
        propertyMap.put("baseBranch", properties.getProperty("baseBranch"));
        propertyMap.put("certificate", properties.getProperty("certificate"));
        propertyMap.put("initialDate", properties.getProperty("initialDate"));

        return propertyMap;
    }

}
